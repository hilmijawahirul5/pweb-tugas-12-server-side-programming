<?php
$arrNilai = array("Fuad" => 80, "ulin" => 90, "Fado" => 75, "Fatan" => 85);

echo "Menampilkan isi array asosiatif dengan foreach: <br>";
foreach ($arrNilai as $nama => $nilai) {
    echo "Nilai $nama = $nilai<br>";
}

reset($arrNilai);
echo "<br>Menampilkan isi array asosiatif dengan WHILE dan LIST:<br>";

$keys = array_keys($arrNilai);
while ($keys) {
    $nama = array_shift($keys);
    $nilai = $arrNilai[$nama];
    echo "Nilai $nama = $nilai<br>";
}
?>
